﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UnidadModulo.Models;

namespace UnidadModulo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnidadController : ControllerBase
    {
        public List<Unidad> listUnidades { get; set; }
        public UnidadController()
        {
            listUnidades = new List<Unidad>()
            {
                new Unidad(){ id = 1, unidad = "UNIDAD", desactivado = "N", usuario = "elieveras" },
                new Unidad(){ id = 2, unidad = "DOCENAS", desactivado = "N", usuario = "elieveras" },
                new Unidad(){ id = 3, unidad = "PAQUETE", desactivado = "N", usuario = "elieveras" }
            };
        }
        // GET: api/Unidad
        [HttpGet]
        public IEnumerable<Unidad> Get()
        {
            return listUnidades;
        }

        // GET: api/Unidad/5
        [HttpGet("{id}")]
        public Unidad Get(int id)
        {
            return listUnidades.Where(x=>x.id ==  id).FirstOrDefault();
        }

        // POST: api/Unidad
        [HttpPost]
        public IActionResult Post([FromBody] Unidad model)
        {
            try
            {
                listUnidades.Add(model);
                return StatusCode(StatusCodes.Status201Created, model);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Unidad/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

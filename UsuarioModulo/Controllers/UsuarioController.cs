﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UsuarioModulo.Models;

namespace UsuarioModulo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsuarioController : ControllerBase
    {
        List<Usuario> listUsuarios { get; set; }
        public UsuarioController()
        {
            listUsuarios = new List<Usuario>()
            {
                new Usuario(){ id = 1, usuario = "everas", desactivado = "N" },
                new Usuario(){ id = 2, usuario = "jdriguez", desactivado = "N" },
                new Usuario(){ id = 3, usuario = "ydiaz", desactivado = "N" }
            };
        }

        // GET: api/Usuario
        [HttpGet]
        public IEnumerable<Usuario> Get()
        {
            return listUsuarios;
        }

        // GET: api/Usuario/5
        [HttpGet("{id}")]
        public Usuario Get(int id)
        {
            return listUsuarios.Where(x=>x.id == id).FirstOrDefault();
        }

        // POST: api/Usuario
        [HttpPost]
        public IActionResult Post([FromBody] Usuario model)
        {
            try
            {
                listUsuarios.Add(model);
                return StatusCode(StatusCodes.Status201Created, listUsuarios);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT: api/Usuario/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
